import java.io.*;

public class SettingsFile {

    public static String[] read() throws FileNotFoundException {
        String Mass[] = new String[2];
        try (FileReader settings = new FileReader(".\\settings.json")) {
            BufferedReader reader = new BufferedReader(settings);
            Mass[0] = reader.readLine();
            Mass[1] = reader.readLine();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return Mass;
    }


    public static void write(String server, String port) throws FileNotFoundException {
        try (FileWriter writer = new FileWriter(".\\settings.json", false)) {
            writer.write(server);
            writer.append('\n');
            writer.write(port);
            writer.append('\n');

            writer.flush();
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
    }


}
