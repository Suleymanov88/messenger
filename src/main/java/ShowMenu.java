import java.io.FileNotFoundException;
import java.util.Scanner;

public class ShowMenu {
    public static void Show() throws FileNotFoundException {
        int i;
        String login, password, server, port;
        String[] parameters = new String[1];
        parameters = SettingsFile.read();
        server = parameters[0];
        port = parameters[1];
        System.out.println("Please, select menu item.");
        System.out.println("1. Autorization");
        System.out.println("2. Registration");
        System.out.println("3. Connection settings");
        System.out.println("4. Exit");
        Scanner sc = new Scanner(System.in);
        switch (sc.next()) {
            case "1":
                System.out.println("Write your login.");
                login = sc.next();
                System.out.println("Write your password."); //надо сделать звездочки
                password = sc.next();
                DBConnect DBConnect = new DBConnect();
                DBConnect.DBconnection(login, password, server, port);
                break;

            case "2":  //Дописать
                System.out.println("Write new login.");
                login = sc.next();
                System.out.println("Write new password."); //надо сделать звездочки
                password = sc.next();
                DBConnect = new DBConnect();
                DBConnect.DBconnection(login, password, server, port);
                break;

            case "3":
                System.out.println("Current connection settings:");
                System.out.println("Server: " + server);
                System.out.println("Port: " + port);
                System.out.println("Please, select menu item.");
                System.out.println("1. change settings");
                System.out.println("2. back");
                switch (sc.next()) {
                    case "1":
                        System.out.println("Write server name / IP address");
                        server = sc.next();
                        System.out.println("Write server port");
                        port = sc.next();
                        SettingsFile.write(server, port);
                        System.out.println("Settings was saved.");
                        ShowMenu.Show();
                        break;

                     case "2":
                        ShowMenu.Show();
                        break;

                    default:
                        System.out.println("Entered is not a valid value");
                        ShowMenu.Show();
                        break;
                }
                break;

            case "4":
                System.exit(0);
                break;

            default:
                System.out.println("Entered is not a valid value");
                ShowMenu.Show();
                break;
        }
    }
}
